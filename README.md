# ReminderBot

ReminderBot is a bot which sends messages into the Slack channel of Workspace.

## Installation

- First Create New App from [Slack api](https://api.slack.com/apps)
- Once you done, click "Incoming Hooks" and turn on
- Scroll down to "Webhook URL" and click "Add New Webhook to Workspace"
- Choose the channel which for to add a ReminderBot
- Copy "Webhook URL" and paste in file SlackMessage.py on line 8th between quotes
```code
    response = requests.post(
        '',
```
- Use the package manager [pip](https://pip.pypa.io/en/stable/) or pip3 to install requests.

```bash
pip install requests
```

## Usage

```python
python SlackMessage.py -m "Message"
```