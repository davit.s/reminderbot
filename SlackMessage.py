import requests
import sys
import getopt

def sendSlackMessage(message):
    payload = '{"text":"%s"}' % message
    response = requests.post(
        '',
        data=payload.encode('utf-8'),
        headers={'Content-type': 'text/plain; charset=utf-8'})
    print(response.text)
     
def main(argv):
    message = '  '
    try: opts, args = getopt.getopt(argv, "hm:", ["message="])
    except getopt.GetoptError:
        print('SlackMessage.py -m <message>')
        sys.exit(2)
    if len(opts) == 0:
        message = "DEFAULT MESSAGE"
    for opt, arg in opts:
         if opt == '-h':
             print('SlackMessage.py -m <message>')
             sys.exit()
         elif opt in ("-m", "message"):
             message = arg

    sendSlackMessage(message)
if __name__ == "__main__":
    main(sys.argv[1:])
